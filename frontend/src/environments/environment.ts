// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

import {HttpHeaders} from "@angular/common/http";

export const environment = {
  production: false
};

export const SKI_API = {
  url: 'https://makevoid-skicams.p.rapidapi.com/cams.json',
  headers: new HttpHeaders({
    "x-rapidapi-host": "makevoid-skicams.p.rapidapi.com",
    "x-rapidapi-key": "f07d18bbf5mshf4053b8a5f5cf5cp14233ejsnf435406d06d2",
    "useQueryString": 'true'
  })
}

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
