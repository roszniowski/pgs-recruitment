import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';
import {ContentLayoutComponent} from "./layout/content-layout/content-layout.component";
import {PageNotFoundComponent} from "./shared/page-not-found/page-not-found.component";

const routes: Routes = [
  {
    path: '',
    redirectTo: 'about',
    pathMatch: 'full'
  },
  {
    path: '',
    component: ContentLayoutComponent,
    children: [
      {
        path: 'about',
        loadChildren: () =>
          import('./modules/about/about.module').then(m => m.AboutModule)
      },
      {
        path: 'skicams',
        loadChildren: () =>
          import('./modules/skicams/skicams.module').then(m => m.SkicamsModule)
      },
      {
        path: 'contact',
        loadChildren: () =>
          import('./modules/contact/contact.module').then(m => m.ContactModule)
      }
    ]
  },
  {path: '**', component: PageNotFoundComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {
}
