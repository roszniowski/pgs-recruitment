import {NgModule} from '@angular/core';
import {ContactRoutingModule} from "./contact-routing.module";
import {SharedModule} from "../../shared/shared.module";
import {ContactComponent} from "./components/contact.component";


@NgModule({
  declarations: [
    ContactComponent
  ],
  imports: [
    ContactRoutingModule,
    SharedModule
  ]
})
export class ContactModule {
}
