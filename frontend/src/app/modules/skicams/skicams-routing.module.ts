import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';

import {SkicamsComponent} from "./components/skicams.component";

const routes: Routes = [
  {
    path: '',
    component: SkicamsComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class SkicamsRoutingModule {
}
