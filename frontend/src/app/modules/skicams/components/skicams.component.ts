import {Component, OnInit} from '@angular/core';
import {SkicamsService} from "../services/skicams.service";
import {of} from "rxjs";
import {filter, map} from "rxjs/operators";

interface ISkicam {
  name: string,
  prov: string,
  cams: [
    {
      name: string
      url: string
    }
  ]
}

@Component({
  selector: 'app-skicams',
  templateUrl: './skicams.component.html',
  styleUrls: ['./skicams.component.scss']
})
export class SkicamsComponent implements OnInit {
  skiCams: ISkicam[] = [];
  date = Date.now();
  isLoading = true;

  constructor(private skicamsService: SkicamsService) {
  }

  ngOnInit(): void {
    this.skicamsService.getSkiCameras().subscribe(
      next => {
        if (!next) return;
        let filteredCams = [];
        next.forEach(item => {
          of(item).pipe(
            filter(val => val['name'] === 'Bielmonte' || val['name'] === 'Livigno'),
            map(cam => {
              return {
                name: cam.name,
                prov: cam.prov,
                cams: Object.values(cam.cams)
              }
            })).subscribe(data => filteredCams.push(data));
        });
        this.skiCams = filteredCams;
        this.isLoading = false;
      },
      error => {
        console.log(error);
        this.isLoading = false;
      }
    )
  }

}
