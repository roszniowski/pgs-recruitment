import {Injectable} from '@angular/core';
import {SkicamsApiService} from "./skicams-api.service";
import {catchError, map} from "rxjs/operators";
import {throwError} from "rxjs";

@Injectable()
export class SkicamsService {

  constructor(private _api: SkicamsApiService) {
  }

  getSkiCameras() {
    return this._api.getSkiCameras().pipe(
      map(object => {
        return Object.values(object);
      }),
      catchError(error => throwError(error.message))
    );
  }
}
