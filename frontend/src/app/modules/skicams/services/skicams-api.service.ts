import {Injectable} from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {SKI_API} from "../../../../environments/environment";

@Injectable()
export class SkicamsApiService {

  constructor(private _http: HttpClient) {
  }

  getSkiCameras() {
    return this._http.get(SKI_API.url, {
      headers: SKI_API.headers
    });
  }
}
