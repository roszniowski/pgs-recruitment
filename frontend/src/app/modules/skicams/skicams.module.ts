import {NgModule} from '@angular/core';
import {SkicamsRoutingModule} from "./skicams-routing.module";
import {SharedModule} from "../../shared/shared.module";
import {SkicamsApiService} from "./services/skicams-api.service";
import {SkicamsService} from "./services/skicams.service";
import {SkicamsComponent} from "./components/skicams.component";
import {HttpClientModule} from "@angular/common/http";


@NgModule({
  declarations: [
    SkicamsComponent
  ],
  imports: [
    SkicamsRoutingModule,
    SharedModule,
    HttpClientModule
  ],
  providers: [
    SkicamsApiService,
    SkicamsService
  ]
})
export class SkicamsModule {
}
