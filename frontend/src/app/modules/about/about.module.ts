import {NgModule} from '@angular/core';
import {AboutRoutingModule} from "./about-routing.module";
import {SharedModule} from "../../shared/shared.module";
import {AboutComponent} from "./components/about.component";


@NgModule({
  declarations: [
    AboutComponent
  ],
  imports: [
    AboutRoutingModule,
    SharedModule
  ]
})
export class AboutModule {
}
